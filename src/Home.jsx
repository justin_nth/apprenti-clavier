import React from 'react';
import { BrowserRouter as Router, Link, Route, Switch } from 'react-router-dom';

import Game from './Game';

import keycodes from './utils/keycodes';
import citations from './utils/citations';

import './css/home.css';

const Home = () => (
  <Router>
    <Switch>
      <Route path="/letters">
        <Game array={keycodes} />
      </Route>
      <Route path="/phrases">
        <Game array={citations} phrases={true} />
      </Route>
      <Route path="/">
        <div className="home">
          <h1 className="title">Apprenti clavier</h1>
          <div className="btn-container">
            <Link className="btn" to="/letters">
              Touches
            </Link>
            <Link className="btn" to="/phrases">
              Phrases
            </Link>
          </div>
        </div>
      </Route>
    </Switch>
  </Router>
);

export default Home;
