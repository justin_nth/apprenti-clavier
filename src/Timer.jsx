import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

const Timer = ({ endGame }) => {
  const [seconds, setSeconds] = useState(0);
  const [minutes, setMinutes] = useState(0);
  const [isActive, setIsActive] = useState(true);

  function stop() {
    setIsActive(false);
  }

  function reset() {
    setSeconds(0);
    setIsActive(false);
  }

  useEffect(() => {
    if (endGame) {
      stop();
    }
    let interval = null;
    if (isActive) {
      interval = setInterval(() => {
        setSeconds(seconds + 1);
        if (seconds === 59) {
          setMinutes(minutes + 1);
          setSeconds(0);
        }
      }, 1000);
    } else if (!isActive && seconds !== 0) {
      clearInterval(interval);
    }
    return () => clearInterval(interval);
  }, [isActive, seconds]);

  return (
    <div className="timer">
      <div className="time">
        {minutes}:{seconds < 10 ? '0' : ''}
        {seconds}
      </div>
    </div>
  );
};

Timer.propTypes = {
  endGame: PropTypes.bool,
};

export default Timer;
