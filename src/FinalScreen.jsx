import React from 'react';
import PropTypes from 'prop-types';

const FinalScreen = ({ points, errors }) => (
  <div className="final-screen">
    <div className="final-score">
      <h4>Félicitations, vous avez terminé le jeu !</h4>
      <p>
        Avec une précision de{' '}
        {isNaN((points / (points + errors)) * 100)
          ? '0'
          : ((points / (points + errors)) * 100).toFixed(2)}
        %
      </p>
    </div>
  </div>
);

FinalScreen.propTypes = {
  points: PropTypes.number.isRequired,
  errors: PropTypes.number.isRequired,
};

export default FinalScreen;
