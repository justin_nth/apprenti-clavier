import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import Characters from './Characters';
import Timer from './Timer';
import FinalScreen from './FinalScreen';
import Phrases from './Phrases';

import keycodes from './utils/keycodes';
import citations from './utils/citations';
import getRandom from './utils/getRandom';

import './css/game.css';

const Game = ({ phrases, array }) => {
  /**
   * CONSTANTS
   */
  const [data, setData] = useState(getRandom(array));

  const [inputLetter, setInputLetter] = useState('');

  const [leftPadding, setLeftPadding] = useState(
    new Array(20).fill(' ').join('')
  );
  const [outgoingChars, setOutgoingChars] = useState('');
  const [currentChar, setCurrentChar] = useState(phrases ? data.charAt(0) : '');
  const [incomingChars, setIncomingChars] = useState(
    phrases ? data.substr(1) : ''
  );

  let [correctPhrases, setCorrectPhrases] = useState(0);

  const [stopGame, setStopGame] = useState(false);

  let [errors, setErrors] = useState(0);
  let [points, setPoints] = useState(0);

  const speaker = new SpeechSynthesisUtterance();

  speaker.lang = 'fr-FR';

  speaker.text = phrases ? data : data.value;

  /**
   * METHODS
   */
  const handleChange = () => {
    if (!phrases) speechSynthesis.speak(speaker);
    setTimeout(() => {
      setInputLetter('');
    }, 300);
  };

  const changePhrase = () => {
    speechSynthesis.speak(speaker);
    setData(getRandom(citations));
    setOutgoingChars('');
    setCurrentChar(data.charAt(0));
    setIncomingChars(data.substring(1));
  };

  const handleKeyDown = (e) => {
    if (phrases) {
      let updatedOutgoingChars = outgoingChars;
      let updatedIncomingChars = incomingChars;

      if (updatedIncomingChars == 0) {
        setCorrectPhrases(correctPhrases + 1);
        if (correctPhrases >= 4) {
          setStopGame(true);
        }
        changePhrase();
      } else if (e.key === currentChar) {
        if (leftPadding.length > 0) {
          setLeftPadding(leftPadding.substring(1));
        }

        setInputLetter(e.key);
        setPoints(points + 1);
        updatedOutgoingChars += currentChar;
        setOutgoingChars(updatedOutgoingChars);

        setCurrentChar(incomingChars.charAt(0));

        updatedIncomingChars = incomingChars.substring(1);
        setIncomingChars(updatedIncomingChars);
      } else setErrors(errors + 1);
    } else {
      if (points >= 20) {
        setStopGame(true);
      } else if (e.charCode == data.key) {
        setPoints(points + 1);
        setData(getRandom(keycodes));
        setInputLetter(data.value);
      } else setErrors(errors + 1);
    }
  };

  return (
    <div className="game">
      <div className="header">
        {phrases ? (
          <h1>Recopiez la phrase suivante</h1>
        ) : (
          <h1>Trouvez les bon caractères</h1>
        )}
        <Timer endGame={stopGame} />
      </div>
      <Link className="btn go-back" to="/">
        Retour à l&apos;accueil
      </Link>

      {stopGame ? (
        <FinalScreen points={points} errors={errors} />
      ) : (
        <div className="two-columns">
          {phrases ? (
            <Phrases
              outgoingChars={outgoingChars}
              currentChar={currentChar}
              incomingChars={incomingChars}
            />
          ) : (
            <Characters className="letter" data={data.value} />
          )}
          <input
            type="text"
            name="letter"
            id="letter-input"
            value={inputLetter}
            autoFocus
            onChange={handleChange}
            onKeyPress={handleKeyDown}
          />
        </div>
      )}
    </div>
  );
};

Game.defaultProps = {
  phrases: false,
};

Game.propTypes = {
  phrases: PropTypes.bool,
  array: PropTypes.array.isRequired,
};

export default Game;
