import React from 'react';
import PropTypes from 'prop-types';

const Phrases = ({ outgoingChars, currentChar, incomingChars }) => {
  return (
    <div className="phrase-container">
      <p className="Character">
        <span className="Character-out">{outgoingChars.slice(-20)}</span>
        <span className="Character-current">{currentChar}</span>
        <span>{incomingChars.substr(0, 30)}</span>
      </p>
    </div>
  );
};

Phrases.propTypes = {
  outgoingChars: PropTypes.string.isRequired,
  currentChar: PropTypes.string.isRequired,
  incomingChars: PropTypes.string.isRequired,
};

export default Phrases;
