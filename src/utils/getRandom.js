function getRandom(array) {
  var random = Math.floor(Math.random() * array.length);
  return array[random];
}

export default getRandom;
