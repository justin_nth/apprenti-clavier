let citations = [
  "Exige beaucoup de toi-meme et attends peu des autres. Ainsi beaucoup d'ennuis te seront épargnés.",

  "La vie c'est des étapes... La plus douce c'est l'amour... La plus dure c'est la séparation... La plus pénible c'est les adieux... La plus belle c'est les retrouvailles.",

  "Dans la vie on ne fait pas ce que l'on veut mais on est responsable de ce que l'on est.",

  "L'une des plus grandes douleurs est d'aimer une personne que tu ne peux pas avoir.",

  "La vie est un mystère qu'il faut vivre, et non un problème à résoudre.",

  "On passe une moitié de sa vie à attendre ceux qu'on aimera et l'autre moitié à quitter ceux qu'on aime.",

  "La vie, c'est comme une bicyclette, il faut avancer pour ne pas perdre l'équilibre.",

  'Pour critiquer les gens il faut les connaitre, et pour les connaitre, il faut les aimer.',

  "La règle d'or de la conduite est la tolérance mutuelle, car nous ne penserons jamais tous de la meme façon, nous ne verrons qu'une partie de la vérité et sous des angles différents.",

  "Un jour l'amour a dit à l'amitié : Pourquoi existes-tu puisque je suis là ? L'amitié lui répond : Pour amener un sourire là ou tu as laissé des larmes.",

  "Choisissez un travail que vous aimez et vous n'aurez pas à travailler un seul jour de votre vie.",

  "Il est difficile de dire adieu lorsqu'on veut rester, compliqué de rire lorsqu'on veut pleurer, mais le plus terrible est de devoir oublier lorsqu'on veut aimer.",

  'Le monde est dangereux à vivre ! Non pas tant à cause de ceux qui font le mal, mais à cause de ceux qui regardent et laissent faire.',

  "On ne souffre jamais que du mal que nous font ceux qu'on aime. Le mal qui vient d'un ennemi ne compte pas.",

  'Le sentiment de ne pas etre aimé est la plus grande des pauvretés.',

  'Un seul etre vous manque et tout est dépeuplé.',

  "Dans la vengeance et en amour, la femme est plus barbare que l'homme.",

  "Aimer, ce n'est pas se regarder l'un l'autre, c'est regarder ensemble dans la meme direction.",

  'La nature fait les hommes semblables, la vie les rend différents.',
  "L'homme veut etre le premier amour de la femme, alors que la femme veut etre le dernier amour de l'homme.",

  "Il ne faut avoir aucun regret pour le passé, aucun remords pour le présent, et une confiance inébranlable pour l'avenir.",

  "Il n'existe que deux choses infinies, l'univers et la betise humaine... mais pour l'univers, je n'ai pas de certitude absolue.",

  "Une femme qu'on aime est toute une famille.",

  "Agis avec gentillesse, mais n'attends pas de la reconnaissance.",

  "La différence qu'il y a entre les oiseaux et les hommes politiques, c'est que de temps en temps les oiseaux s'arretent de voler !",

  "L'honneteté, la sincérité, la simplicité, l'humilité, la générosité, l'absence de vanité, la capacité à servir les autres - qualités à la portée de toutes les ames- sont les véritables fondations de notre vie spirituelle.",

  "Lorsque l'on se cogne la tete contre un pot et que cela sonne creux, ça n'est pas forcément le pot qui est vide.",

  "Trois sortes d'amis sont utiles, trois sortes d'amis sont néfastes. Les utiles : un ami droit, un ami fidèle, un ami cultivé. Les néfastes : un ami faux, un ami mou, un ami bavard.",

  "Le bonheur c'est lorsque vos actes sont en accord avec vos paroles.",

  "La distance n'est pas un obstacle mais un beau rappel de la force du véritable amour.",

  "Ne pleure jamais pour quelqu'un dans ta vie, parce que ceux pour qui tu pleures ne méritent pas tes larmes et ceux qui les méritent, ne te laisseraient jamais pleurer.",

  "La déception ne vient jamais des autres, elle n'est que le reflet de nos erreurs de jugement.",

  "Un sourire coute moins cher que l'électricité, mais donne autant de lumière.",

  "La valeur d'un homme ne se mesure pas à son argent, son statut ou ses possessions. La valeur d'un homme réside dans sa personnalité, sa sagesse, sa créativité, son courage, son indépendance et sa maturité.",

  "L'amour c'est la générosité du coeur, il ne connait pas l'apparence physique, ni l'argent, c'est le fait de penser à quelqu'un malgré la distance ; ce qui fait le plus souffrir c'est le mensonge et les faux espoirs.",

  "L'expérience prouve que celui qui n'a jamais confiance en personne ne sera jamais déçu.",
];

export default citations;
