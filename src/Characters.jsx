import React from 'react';
import PropTypes from 'prop-types';

const Characters = ({ className, data }) => {
  return (
    <div className={className}>
      <h3>{data}</h3>
    </div>
  );
};

Characters.propTypes = {
  className: PropTypes.string,
  data: PropTypes.string.isRequired,
};

export default Characters;
